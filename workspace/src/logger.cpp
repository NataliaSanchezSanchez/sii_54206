//Autora: Natalia Sánchez


#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h> 
#include <unistd.h>
 
int main(int argc,char* argv[]) 
{
	char buf[40];
	int nread;
	//borro fifo por si existía antes
	//unlink("fifo");
	//creo la tubería
	if( mkfifo( "fifo" ,O_RDWR)!=0)
	{	perror("No se puede crear la tuberia");
		return 1;
	}		
	
	//se abre el modo lectura del fifo creado
	int mififo=open("fifo", O_RDONLY);
	
	while((nread=read(mififo,buf,40))>0)
		{printf("%s", buf);}

	if(nread<0)
		{perror("read");return 1;}
		
	close(mififo);
	
	int rent=unlink("fifo");
	if(rent<0)
	{	perror("Error al eliminar tuberia");
		return 1;}
	return 0;
}

