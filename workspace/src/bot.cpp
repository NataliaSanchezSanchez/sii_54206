//AUTORA: NATALIA SÁNCHEZ
#pragma once

#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h> 
#include <unistd.h>
#include <iostream>
#include <sys/mman.h>

int main(void)
{
	DatosMemCompartida *datosmem;//puntero a memoria compartida
	char *org;
	
	
	//Abro el fichero 
	int fdm=open("DatosMemCompartida.txt",O_RDWR);
	
	//proyectamos el fichero
	org=(char*)mmap(NULL,sizeof(*(datosmem)),PROT_WRITE|PROT_READ,MAP_SHARED,fdm,0);
	close(fdm);
	datosmem=(DatosMemCompartida *)org;
	
	//bucle infinito
	while(1)
	{
		float posicionraqueta1_x,posicionraqueta1_y;
		float posicionraqueta2_x,posicionraqueta2_y;
		posicionraqueta1_y=(datosmem->raqueta1.y2+datosmem->raqueta1.y1)/2;
		posicionraqueta1_x=(datosmem->raqueta1.x1);
		posicionraqueta2_y=(datosmem->raqueta2.y2+datosmem->raqueta2.y1)/2;
		posicionraqueta2_x=(datosmem->raqueta2.x1);
		///Código para que las raquetas funcionen como bot
		if((datosmem->esfera.centro.x<0)&&(datosmem->esfera.velocidad.x<0))
	{
		if(posicionraqueta1_y<datosmem->esfera.centro.y)
			datosmem->accion1=1;
		else if(posicionraqueta1_y>datosmem->esfera.centro.y)
			datosmem->accion1=-1;
		else
			datosmem->accion1=0;
		
		usleep(25000);	
	}
	
	if((datosmem->esfera.centro.x>0)&&(datosmem->esfera.velocidad.x>0))
	{
		if(posicionraqueta2_y<datosmem->esfera.centro.y)
			datosmem->accion2=1;
		else if(posicionraqueta2_y>datosmem->esfera.centro.y)
			datosmem->accion2=-1;
		else
			datosmem->accion2=0;
		
		usleep(25000);	
	}
}
	//fin de la proyeccion
	munmap(org,sizeof(*(datosmem)));
	
	return 0;
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
